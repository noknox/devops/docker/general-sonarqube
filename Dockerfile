FROM openjdk:11-jre-slim-buster

RUN apt update && \
    apt install -y unzip wget && \
    wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.2.0.1873-linux.zip -O /tmp/sonar-scanner.zip && \
    unzip /tmp/sonar-scanner.zip -d /opt && \
    rm /tmp/sonar-scanner.zip && \
    ln -s /opt/sonar-scanner-4.2.0.1873-linux/bin/sonar-scanner /usr/local/bin/sonar-scanner && \
    apt remove -y unzip wget && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*